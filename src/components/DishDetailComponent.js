import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb, BreadcrumbItem, Row, Button, Label, Modal, ModalBody, ModalHeader, Col } from 'reactstrap';
import { Control, LocalForm, Errors } from 'react-redux-form';
import { Link } from 'react-router-dom';

const required = (val) => val && val.length;
const maxLength = (len) => (val) => !(val) || (val.length <= len);
const minLength = (len) => (val) => (val) && (val.length >= len);

class CommentForm extends Component { 

  constructor(props) {
    super(props);
    this.state = {
      isCommentZoneOpen: false
    };
    this.toggleCommentZone = this.toggleCommentZone.bind(this);
    this.hangleCommentZone = this.hangleCommentZone.bind(this);
  }

  toggleCommentZone() {
    this.setState({
      isCommentZoneOpen: !this.state.isCommentZoneOpen
    });
  }

  hangleCommentZone (values) {
    this.toggleCommentZone();
    console.log("Current State is:" + JSON.stringify(values));
    alert("Current State is:" + JSON.stringify(values));
  }

  render() {
    return(
      <div>
        <Button outline onClick={this.toggleCommentZone}>
          <span className="fa fa-pencil fa-lg">Submit Comment</span>
        </Button>

        <Modal isOpen={this.state.isCommentZoneOpen} toggle={this.toggleCommentZone}>
            <ModalHeader toggle={this.toggleCommentZone}>Submit Comment</ModalHeader>
            <ModalBody> 
              <LocalForm onSubmit={(values) => this.hangleCommentZone(values)}>
                <Row className="form-group">
                  <Col md={12}>
                    <Label htmlFor="rating">Rating</Label>
                    <Control.select model=".rating" id="rating" name="rating" className="form-control">
                      <option>1</option>
                      <option>2</option>
                      <option>3</option>
                      <option>4</option>
                      <option>5</option>
                    </Control.select>
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col md={12}>
                    <Label htmlFor="name">Your Name</Label>
                    <Control.text model=".name" id="name" name="name"
                      placeholder="Your Name"
                      className="form-control"
                      validators={{
                        required, minLength: minLength(3), maxLength: maxLength(15) 
                      }}
                      />
                    <Errors
                      className="text-danger"
                      model=".name"
                      show="touched"
                      messages={{
                        required: 'Required',
                        minLength: 'Must be greater than 2 characters',
                        maxLength: 'Must be 15 characters or less'
                      }}
                    />
                  </Col>
                </Row>
                <Row className="form-group">
                  <Col md={12}>
                    <Label htmlFor="comment">Comment</Label>
                    <Control.textarea model=".message" id="comment" name="comment"
                      placeholder="message..." 
                      rows="12" 
                      className="form-control" 
                    />
                  </Col>
                </Row>
                <Button type="submit" value="submit" className="primary">Submit Comment</Button>
              </LocalForm>
            </ModalBody>
        </Modal>
      </div>
    );
  }
}

  function RenderDish({dish}) {
      return (
        <div className="col-xs-12 col-md-5 m-1">
          <Card>
            <CardImg top with="100%" src={dish.image} alt={dish.name} />
            <CardBody>
              <CardTitle>{dish.name}</CardTitle>
              <CardText>{dish.description}</CardText>
            </CardBody>
          </Card>
        </div>
      );
  }

  function RenderComments({comments}) {
    if (comments != null) {
      return (
          <div className="col-xs-12 col-md-5 m-1">
            <h4>Comments</h4>
            <ul className="list-unstyled">
              {comments.map((comment) => {
                return (
                  <li key={comment.id}>
                    <p>{comment.comment}</p>
                    <p>--{comment.author}, {new Intl.DateTimeFormat('en-US', {year: 'numeric', month: 'short', day: '2-digit'}).format(new Date(Date.parse(comment.date)))}</p>
                  </li>
                );
              })}
            </ul>
            <CommentForm />
          </div>
        );
    }
    else {
      return (
       <div></div>
     );
   }
  }

  const DishDetail = (props) => {
    if (props.dish != null) {
      return (
        <div className="container">
          <div className="row">
            <Breadcrumb>
              <BreadcrumbItem><Link to={'/menu'}>Menu</Link></BreadcrumbItem>
              <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
            </Breadcrumb>
            <div className="col-12">
              <h3>{props.dish.name}</h3>
              <hr />
            </div>
          </div>
          <div className="row">
            <RenderDish dish={props.dish} />
            <RenderComments comments={props.comments} />
          </div>
        </div>
      );
    }
    else {
      return (
       <div></div>
     );
   }
  }

export default DishDetail;
